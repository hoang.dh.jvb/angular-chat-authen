"use strict";
var User = (function () {
    function User(username, password, name) {
        this.username = username;
        this.password = password;
        this.name = name;
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.model.js.map