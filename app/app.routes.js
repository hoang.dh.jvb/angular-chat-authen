"use strict";
var router_1 = require("@angular/router");
var message_component_1 = require("./message.component");
var login_component_1 = require("./login.component");
var authentication_guard_1 = require("./guards/authentication.guard");
var routing = [
    { path: '', component: message_component_1.MessageListComponent, canActivate: [authentication_guard_1.AuthGuard] },
    { path: 'login', component: login_component_1.LoginComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
exports.appRoutes = router_1.RouterModule.forRoot(routing);
//# sourceMappingURL=app.routes.js.map