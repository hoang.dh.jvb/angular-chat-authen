import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService} from './services/authentication.service';

@Component ({
    selector: 'login-component',
    templateUrl: 'app/login.component.html'
})
export class LoginComponent  {
    constructor (private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService) {}

        returnUrl: string;

        //Init login component
        ngOnInit() {
            // reset login status
            this.authenticationService.logout();

            // // get return url from route parameters or default to '/'
            // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        }


        CheckLogin(values: any) {
            this.authenticationService.login(values.username, values.password);
        }
}