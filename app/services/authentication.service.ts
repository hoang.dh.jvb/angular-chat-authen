import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {Router} from '@angular/router';
import {User} from '../models/user.model';

@Injectable()
export class AuthenticationService {
    constructor(private router: Router) { }

    login(username: string, password: string) {
        var user: User;
        if(username == "admin"  && password == "1234"){
            let user = new User(username,password,'test01');
            console.log(user);
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.router.navigate(['/']);
        }
        
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}