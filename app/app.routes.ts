import { Routes, RouterModule } from '@angular/router';
import { MessageListComponent } from './message.component';
import { LoginComponent } from './login.component';
import {AuthGuard} from './guards/authentication.guard';
const routing: Routes = [
    {path: '', component: MessageListComponent,canActivate: [AuthGuard] },
    {path: 'login', component: LoginComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
]
export const appRoutes = RouterModule.forRoot(routing);